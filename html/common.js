var apiUrl = "api";

function isEmpty(value) {
    if (value == null || value == undefined || (value + "").trim().length < 1) {
        return true;
    }
    return false;
};

function sendGet(url) {
    let result;
	var sendurl = apiUrl + url;
	console.log("sendurl "+sendurl);
    $.ajax({
        url: sendurl, //接口
        type: "get", //GET或POST
        async: false,
        success: function (obj) {
            result = obj;
        },
    });
    console.log("调用后台接口：" + url + "    返回信息：" + result);
    return result;
}