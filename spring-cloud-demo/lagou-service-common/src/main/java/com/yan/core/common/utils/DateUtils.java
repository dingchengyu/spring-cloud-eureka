package com.yan.core.common.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author 汪焰
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

//        <dependency>
//            <groupId>org.apache.commons</groupId>
//            <artifactId>commons-lang3</artifactId>
//            <version>3.9</version>
//        </dependency>

    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYYMMDD = "yyyyMMdd";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss",
            "yyyy/MM/dd HH:mm", "yyyy/MM", "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    public static String START = " 00:00:00";

    /**
     * date1 <= date2 返回true
     *
     * @Author: 汪焰
     * @Date: 2020/7/17 14:23
     * version 1.0.0
     **/
    public static boolean lte(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return true;
        }
        if (date1 == null || date2 == null) {
            return false;
        }
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        if (time1 <= time2) {
            return true;
        }
        return false;
    }

    /**
     * 获取当前Date型日期
     *
     * @return Date() 当前日期
     */
    public static Date getDate() {
        return new Date();
    }

    /**
     * 获取 n 天前早上, 默认格式为yyyy-MM-dd 00:00:00
     *
     * @return String
     */
    public static Date getNDateStart(int n) {
        return parseDate(getDayAfter(n) + START);
    }

    public static final String getTime() {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd 00:00:00
     *
     * @return String
     */
    public static String getTimeStart() {
        return dateTimeNow(YYYY_MM_DD) + START;
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     *
     * @return String
     */
    public static String getTimeDay() {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String parseDateToStr(final String format, final Date date) {
        return new SimpleDateFormat(format).format(date);
    }

    public static final String dateTimeNow(final String format) {
        return parseDateToStr(format, getDate());
    }

    /**
     * 获取 n 天后日期, 默认格式为yyyy-MM-dd
     * n > 0 ===>>>> n天后
     * n < 0 ===>>>> n天前
     *
     * @return String
     */
    public static String getDayAfter(int n) {
        return parseDateToStr(YYYY_MM_DD, getDayDateAfter(n));
    }

    /**
     * 获取 n 分钟后的时间 默认格式为 yyyy-MM-dd HH:mm:ss
     * n > 0 ===>>>> n分钟后
     * n < 0 ===>>>> n分钟前
     *
     * @return String
     */
    public static String getMinuteAfter(int n) {
        return parseDateToStr(YYYY_MM_DD, getDayDateAfter(n));
    }

    /**
     * 获取 n 天后日期, 默认格式为 yyyy-MM-dd
     * n > 0 ===>>>> n天后
     * n < 0 ===>>>> n天前
     *
     * @return Date
     */
    public static Date getDayDateAfter(int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, n);
        return calendar.getTime();
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath() {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime() {
        Date now = new Date();
        return DateFormatUtils.format(now, YYYYMMDD);
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str) {
        if (str == null) {
            return null;
        }
        try {
            return parseDate(str.toString(), parsePatterns);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate() {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }
}