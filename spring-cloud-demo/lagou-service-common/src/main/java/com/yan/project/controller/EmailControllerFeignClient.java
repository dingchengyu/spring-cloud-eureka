package com.yan.project.controller;

import com.yan.project.controller.exception.EmailFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/26 11:33
 */
//@FeignClient(value = "LAGOU-SERVICE-EMAIL", path = "/email")
@FeignClient(value = "LAGOU-SERVICE-EMAIL", fallback = EmailFeignClientFallback.class, path = "/email")
public interface EmailControllerFeignClient {

    //Feign要做的事情就是 拼接url发起请求
    @GetMapping("/{email}/{code}")
    public Boolean email(@PathVariable("email") String email, @PathVariable("code") String code);
}
