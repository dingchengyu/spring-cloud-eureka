package com.yan.project.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author WangYan
 * @description: 验证码存储表
 * @date 2021/3/25 17:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lagou_auth_code")
public class EmailCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String code;
    private Date createtime;
    private Date expiretime;

    public EmailCode(String email, String code, Date createtime, Date expiretime) {
        this.email = email;
        this.code = code;
        this.createtime = createtime;
        this.expiretime = expiretime;
    }
}
