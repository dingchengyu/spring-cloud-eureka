package com.yan.project.controller.exception;

import com.yan.project.controller.EmailControllerFeignClient;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @Author: 汪焰
 * @Date: 2021/3/26 21:52
 */
@Component
public class EmailFeignClientFallback implements EmailControllerFeignClient {
    @Override
    public Boolean email(String email, String code) {
        return false;
    }
}
