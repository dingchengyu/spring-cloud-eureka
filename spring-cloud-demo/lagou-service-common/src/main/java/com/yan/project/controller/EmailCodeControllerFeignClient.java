package com.yan.project.controller;

import com.yan.project.controller.exception.EmailCodeFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * TODO
 *
 * @Author: 汪焰
 * @Date: 2021/3/26 22:48
 */
@FeignClient(value = "LAGOU-SERVICE-CODE", fallback = EmailCodeFeignClientFallback.class, path = "/code")
public interface EmailCodeControllerFeignClient {

    //校验验证码是否正确，0正确1错误2超时
    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable("email") String email, @PathVariable("code") String code);

}
