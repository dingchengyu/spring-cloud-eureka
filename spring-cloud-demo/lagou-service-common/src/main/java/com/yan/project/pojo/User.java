package com.yan.project.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author WangYan
 * @description: 用户表
 * @date 2021/3/25 17:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lagou_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String password;
    private Integer isDelete;
    private Date createTime;
    private Date updateTime;

    public User(String email) {
        this.email = email;
    }

    public User(String email, String password, Integer isDelete, Date createTime, Date updateTime) {
        this.email = email;
        this.password = password;
        this.isDelete = isDelete;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}
