package com.yan.project.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author WangYan
 * @description: 令牌存储表
 * @date 2021/3/25 17:55
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "lagou_token")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String token;

    public Token(String email, String token) {
        this.email = email;
        this.token = token;
    }
}
