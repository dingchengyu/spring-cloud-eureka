package com.yan.project.controller.exception;

import com.yan.project.controller.EmailCodeControllerFeignClient;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @Author: 汪焰
 * @Date: 2021/3/26 21:52
 */
public class EmailCodeFeignClientFallback implements EmailCodeControllerFeignClient {
    @Override
    public Integer validate(String email, String code) {
        return 1;
    }
}
