package com.yan.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ip 放爆刷
 *
 * @Author: 汪焰
 * @Date: 2021/3/28 17:25
 */
@Slf4j
@Component
public class IpRiotBrushFilter implements GlobalFilter, Ordered {


    /**
     * 过滤器封装了核心方法
     *
     * @Author: 汪焰
     * @Date: 2021/3/28 16:56
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String clientIp = request.getRemoteAddress().getHostString();
        //判断是否 爆刷的ip
        if (isRiotBrushIp(clientIp)) {
            ServerHttpResponse response = exchange.getResponse();
            //拒绝访问
            response.setStatusCode(HttpStatus.UNAUTHORIZED);//状态码
            String data = "你的请求过于频繁 请稍后再试!";
            DataBuffer wrap = response.bufferFactory().wrap(data.getBytes());
            System.out.println("gateway IpRiotBrushFilter 是爆刷ip，将被拒绝访问！" + clientIp);
            return response.writeWith(Mono.just(wrap));
        }
        System.out.println("gateway IpRiotBrushFilter 没问题");
        return chain.filter(exchange);
    }

    /**
     * 返回值表示当前过滤器的顺序（优先级），数值越小，优先级越高
     *
     * @Author: 汪焰
     * @Date: 2021/3/28 16:57
     */
    @Override
    public int getOrder() {
        return 0;
    }

    private static Map<String, List<Long>> ipRiotBrushMap = new HashMap<>();

    //爆刷的 限制时间单位 S
    private Integer limitTime = 1;
    //爆刷的 限制 限制时间单位 请求个数
    private Integer limitReqNum = 5;

    /**
     * 判断该ip是否爆刷了我们的服务
     *
     * @Author: 汪焰
     * @Date: 2021/3/28 17:48
     */
    private boolean isRiotBrushIp(String ip) {
        List<Long> reqLogs = getReqLogs(ip);
        int ipReqNum = reqLogs.size();
        if (ipReqNum < limitReqNum) {
            return false;
        }
        //拿到限制时间的起始位置
        long limitStartTime = System.currentTimeMillis() - this.limitTime * 1000;//转成毫秒
        int limitIpReqNum = 0;
        while (true) {
            int index = ipReqNum - limitIpReqNum - 1;
            if (index < 0 || reqLogs.get(index) < limitStartTime) {
                break;
            }
            limitIpReqNum++;
        }
        if (limitReqNum > limitIpReqNum) {
            return false;
        }
        return true;
    }

    //将请求记录下来 并返回所有请求记录
    private List<Long> getReqLogs(String ip) {
        //将请求记录下来
        List<Long> times = ipRiotBrushMap.get(ip);
        if (times == null) {
            times = new ArrayList<>();
        }
        long thisTime = System.currentTimeMillis();
        times.add(thisTime);
        ipRiotBrushMap.put(ip, times);
        return times;
    }

}
