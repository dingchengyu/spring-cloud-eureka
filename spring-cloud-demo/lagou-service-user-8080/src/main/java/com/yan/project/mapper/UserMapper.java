package com.yan.project.mapper;

import com.yan.project.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/25 17:43
 */
public interface UserMapper extends JpaRepository<User, Long> {

}
