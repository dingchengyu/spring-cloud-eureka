package com.yan.project.mapper;

import com.yan.project.pojo.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/25 17:43
 */
public interface TokenMapper extends JpaRepository<Token, Long> {

    @Query(value = "SELECT email FROM lagou_token WHERE token=?1", nativeQuery = true)
    String queryLastEmail(String token);

}
