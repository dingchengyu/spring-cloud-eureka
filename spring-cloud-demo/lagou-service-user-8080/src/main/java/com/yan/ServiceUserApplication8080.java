package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/15 15:36
 */
@SpringBootApplication
@EntityScan("com.yan.project.pojo")
@EnableDiscoveryClient //开启 服务注册中心
@EnableFeignClients//开启 Feign 客户端功能
public class ServiceUserApplication8080 {

    public static void main(String[] args) {
        SpringApplication.run(ServiceUserApplication8080.class, args);
    }

}
