package com.yan.project;

import com.yan.project.service.EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailSpringbootApplicationTest {


    @Autowired
    EmailService emailService;


    @Test
    public void contextLoads() {
    }

    @Test
    public void sayHellTest(){
        emailService.sayHello();
    }

    @Test
    public void sendTextMailTest(){
        emailService.sendTextMail(new String[]{"1249774208@qq.com"},"邮件测试","This sendTextMailTest");
    }

    @Test
    public void sendHtmlMailTest(){
        String html = "<html>"+"<h3>这是HTML邮件</h3><html>";

        emailService.sendHtmlMail(new String[]{"1249774208@qq.com"},"邮件测试",html);
    }

    @Test
    public void sendAttachmentsMailTest(){
        String []files = new String[]{"F:/test/1.png","F:/test/2.jpg","F:/test/3.png"};
        emailService.sendAttachmentsMail(new String[]{"1249774208@qq.com"},"邮件测试","This sendAttachmentsMail",files);
    }



    @Test
    public void sendTemplateMailTest(){
        Map<String,Object> variables = new HashMap<>();
        variables.put("id","123");
        emailService.sendTemplateMail(new String[]{"1249774208@qq.com"},"邮件测试","templateMail",variables);
    }

}
