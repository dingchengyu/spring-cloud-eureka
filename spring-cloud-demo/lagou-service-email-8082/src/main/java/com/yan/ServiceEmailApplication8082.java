package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/15 17:25
 */
@SpringBootApplication
@EnableDiscoveryClient//开启 服务注册中心
public class ServiceEmailApplication8082 {

    public static void main(String[] args) {
        SpringApplication.run(ServiceEmailApplication8082.class, args);
    }

}
