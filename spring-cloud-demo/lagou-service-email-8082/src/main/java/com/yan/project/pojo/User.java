package com.yan.project.pojo;

import lombok.Data;

/**
 * @description: 用户表
 * @date 2021/3/25 17:55
 */
@Data
public class User {
    private Long id;
    private String email;
    private String password;
    private String token;
}
