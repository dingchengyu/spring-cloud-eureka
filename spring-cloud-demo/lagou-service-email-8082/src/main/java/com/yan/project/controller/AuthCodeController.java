package com.yan.project.controller;

import com.yan.project.pojo.AuthCode;
import com.yan.project.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authcode")
public class AuthCodeController {

    @Autowired
    private AuthCodeService authCodeService;

    @RequestMapping("/getCode")
    public int getCode(String email){
        AuthCode authCode = new AuthCode();
        authCode.setEmail(email);
        authCode.setCode("1125");
        return authCodeService.addAuthCode(authCode);
    }
}
