package com.yan.project.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @description: 验证码存储表
 * @date 2021/3/25 17:55
 */
@Data
public class AuthCode {
    private String email;
    private String code;
    private Date createtime;
    private Date expiretime;
}
