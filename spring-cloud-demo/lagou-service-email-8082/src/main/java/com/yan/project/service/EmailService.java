package com.yan.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;

@Service
public class EmailService {

    @Value("${spring.mail.username}")
    private String fromWho;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine templateEngine;

    public void sayHello(){
        System.out.println("Hell word");
    }

    public void sendTextMail(String []to ,String subject, String text){
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(to);
            mailMessage.setSubject(subject);
            mailMessage.setText(text);
            mailMessage.setFrom(fromWho);
            mailSender.send(mailMessage);
        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public void sendHtmlMail(String []to ,String subject, String text) {
        try {
            MimeMessage message = mailSender.createMimeMessage();

            //true表示需要创建一个multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(fromWho);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, true);

            mailSender.send(message);
            System.out.println("发送HTMLMail成功~");
        } catch (MessagingException e) {
            e.printStackTrace();
            System.out.println("发送HTMLMail失败~");
        }
    }

    public void sendAttachmentsMail(String []to, String subject, String text,String []files) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            helper.setFrom(fromWho);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, true);

            //添加附件信息
            if (files!=null && files.length>0){
                for (String filePath:files){
                    File file = new File(filePath);
                    if (file.exists()){
                        helper.addAttachment(file.getName(),file);
                    }
                }
            }

            mailSender.send(message);
            System.out.println("发送AttachmentsMail成功！");
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("发送AttachmentsMail失败！");
        }
    }

    public void sendTemplateMail(String []to,String subject,String templateName,Map<String,Object> variables) {
        Context context = new Context();
        if (variables!=null && !variables.isEmpty()){
            for (String key : variables.keySet()){
                context.setVariable(key,variables.get(key));
            }
        }
        String content = templateEngine.process(templateName,context);
        sendHtmlMail(to,subject,content);
    }
}
