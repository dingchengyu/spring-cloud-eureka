package com.yan.project.controller;

import com.yan.component.EmailApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/15 16:11
 */
@RestController
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private EmailApi emailApi;

    /**
     * 发送验证码到邮箱，true成功，false失败
     *
     * @Author WangYan
     * @Date 2021/3/15 16:15
     */
    @GetMapping("/{email}/{code}")
    public Boolean email(@PathVariable("email") String email, @PathVariable("code") String code) {
        System.out.println("调用了后台接口 email" + email + "----------" + code);
        emailApi.sendSimpleMail(email, "验证码", "你的验证：" + code);
        return true;
    }

}
