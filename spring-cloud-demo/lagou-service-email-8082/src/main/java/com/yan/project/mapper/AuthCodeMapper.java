package com.yan.project.mapper;

import com.yan.project.pojo.AuthCode;

public interface AuthCodeMapper {
    int addAuthCode(AuthCode authCode);
}
