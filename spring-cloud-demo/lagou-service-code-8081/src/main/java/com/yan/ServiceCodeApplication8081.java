package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/15 16:22
 */
@SpringBootApplication
@EnableDiscoveryClient //开启 服务注册中心
@EnableFeignClients//开启 Feign 客户端功能
@EntityScan("com.yan.project.pojo")
public class ServiceCodeApplication8081 {

    public static void main(String[] args) {
        SpringApplication.run(ServiceCodeApplication8081.class, args);
    }

}