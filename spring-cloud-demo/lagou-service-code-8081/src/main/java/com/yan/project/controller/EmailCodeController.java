package com.yan.project.controller;

import com.yan.project.service.EmailCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/15 16:11
 */
@RestController
@RequestMapping("/code")
public class EmailCodeController {

    @Autowired
    private EmailCodeService emailCodeService;

    /**
     * ⽣成验证码并发送到对应邮箱，成功true，失败false
     *
     * @Author WangYan
     * @Date 2021/3/15 16:15
     */
    @GetMapping("/create/{email}")
    public Boolean create(@PathVariable("email") String email) {
        System.out.println("进入到后台接口了");
        return emailCodeService.create(email);
    }

    /**
     * 校验验证码是否正确，0正确1错误2超时
     *
     * @Author WangYan
     * @Date 2021/3/15 16:15
     */
    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable("email") String email, @PathVariable("code") String code) {
        try {
            return emailCodeService.validate(email, code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 2;
    }


}
