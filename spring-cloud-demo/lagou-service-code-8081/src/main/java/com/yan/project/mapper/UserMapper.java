package com.yan.project.mapper;

import com.yan.project.pojo.User;

public interface UserMapper {
    int save(User user);
}
