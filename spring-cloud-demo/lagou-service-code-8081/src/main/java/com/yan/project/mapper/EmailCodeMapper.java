package com.yan.project.mapper;

import com.yan.project.pojo.EmailCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/25 18:03
 */
public interface EmailCodeMapper extends JpaRepository<EmailCode, Long> {

    @Query(value = "select c.code from lagou_auth_code c where c.email=?1 and c.expiretime>now() order by c.createtime desc limit 1", nativeQuery = true)
    String queryValidCode(String email);

}
