package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/23 16:06
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer // 开启配置服务器功能
public class ConfigApplication9006 {

    //测试访问地址
    //http://localhost:9006/master/lagou-springcloud-homework.yml

    public static void main(String[] args) {
        SpringApplication.run(ConfigApplication9006.class, args);
    }

}