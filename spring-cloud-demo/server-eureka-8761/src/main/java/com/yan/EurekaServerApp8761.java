package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author WangYan
 * @description:
 * @date 2021/3/23 15:56
 */
@SpringBootApplication
//声明当前项目为eureka服务
@EnableEurekaServer
public class EurekaServerApp8761 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApp8761.class, args);
    }

}